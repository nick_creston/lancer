<?php

namespace app\controllers;

use Yii;
use yii\rest\Controller;
use app\models\LoginForm;
use app\models\RegisterForm;

class SiteController extends Controller
{
    public function actionIndex() {
        return 'api';
    }

    public function actionLogin() {
        $model = new LoginForm();
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->login()) {
            return ['success'=> true, 'access_token' => Yii::$app->user->identity->getAccessToken()];
        } else {
            Yii::$app->response->statusCode = 422;
            $model->validate();
            return ['success' => false, 'errors'=> $model->getErrors()];
        }

    }

    public function actionRegister() {
        $model = new RegisterForm();
        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->register()) {
            return ['success' => true];
        }
        else {
            $model->validate();
            return ['success' => false, 'errors'=> $model->getErrors()];
        }
    }

    protected function verbs()
    {
        return [
            'login' => ['post'],
            'register' => ['post']
        ];
    }
}
