<?php

namespace app\controllers;
use app\models\Task;
use Yii;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\AccessControl;

class TaskController extends ActiveController
{
    public $modelClass = 'app\models\Task';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['only'] = ['close', 'execute'];
        $behaviors['authenticator']['authMethods'] = [
            HttpBasicAuth::className(),
            HttpBearerAuth::className(),
        ];
//        $behaviors['access'] = [
//            'class' => AccessControl::className(),
//            'only' => ['close'],
//            'rules' => [
//                [
//                    'allow' => true,
//                    'roles' => ['@'],
//                ],
//            ],
//        ];
        return $behaviors;
    }

    public function actionClose() {
        return ['user'=> Yii::$app->user->id];
    }

    public function actionExecute() {
        $params = Yii::$app->getRequest()->getBodyParams();
        $task = Task::findOne(['id' => (int)$params['id']]);

        if($task) {
            if($task->status === Task::STATUS_NEW) {
                $task->status = Task::STATUS_EXECUTE;
                $task->executor_id = Yii::$app->user->id;
                $task->save();
                return ['success' => true, 'message' => 'Task is executed', 'executor_id' => $task->executor_id];
            }
            else {
                return ['success' => false, 'message' => 'Task is not new'];
            }
        }

        return ['success' => false, 'message' => 'Task is not found'];


    }


    protected function verbs()
    {
        return [
            'execute' => ['post'],
        ];
    }
}