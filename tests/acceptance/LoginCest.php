<?php

use app\tests\fixtures\UserFixture;

class LoginCest
{
    public function _before(\AcceptanceTester $I)
    {
        $I->haveFixtures([
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
        ]);
    }
    public function badMethod(\AcceptanceTester $I)
    {
        $I->sendGET('/login');
        $I->seeResponseCodeIs(405);
        $I->seeResponseIsJson();
    }

    public function wrongCredentials(\AcceptanceTester $I)
    {
        $I->sendPOST('/login', [
            'username' => 'wrong_user',
            'password' => 'wrong-password',
        ]);
        $I->seeResponseCodeIs(422);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'success' => false,
            'errors' => [
                'password' => ["Incorrect username or password."]
            ]
        ]);
    }

    public function success(\AcceptanceTester $I)
    {
        $I->sendPOST('/login', [
            'username' => 'nader.katarina',
            'password' => 'password_0',
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('$.access_token');
    }

}
