<?php

use app\tests\fixtures\TaskFixture;
use app\tests\fixtures\UserFixture;
class TaskCest
{
    public function _before(\AcceptanceTester $I)
    {
        $I->haveFixtures([
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'task' => [
                'class' => TaskFixture::className(),
                'dataFile' => codecept_data_dir() . 'task.php'
            ],
        ]);
    }

    public function listTasks(\AcceptanceTester $I) {
        $I->sendGET('/tasks');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            [
                'id' => 1,
                'name' => 'Velit ab odio dolor.',
                'description' => 'Consectetur enim iste aspernatur modi voluptatem commodi consequatur magnam. Nam architecto vel culpa. Consequatur voluptates molestias harum placeat est similique.',
                'status' => 20,
                'cost' => 0,
                'owner_id' => 1,
                'executor_id' => 2,
                'created_at' => 1552385960,
                'updated_at' => 1552385960,
            ],
            [
                'id' => 2,
                'name' => 'Nisi tenetur.',
                'description' => 'Ut nostrum distinctio vero velit est. Quo consequuntur harum repellendus voluptas tempore consectetur maxime vitae. Illo commodi et odit magni et omnis animi.',
                'status' => 10,
                'cost' => 0,
                'owner_id' => 1,
                'executor_id' => 2,
                'created_at' => 1552385960,
                'updated_at' => 1552385960,
            ],
            [
                'id' => 3,
                'name' => 'Sed ratione aut in.',
                'description' => 'Impedit repellat et fugiat sit minus tempora. Necessitatibus autem autem commodi. Explicabo sit eum maxime doloribus quidem. Et blanditiis doloribus aliquid ut.',
                'status' => 30,
                'cost' => 0,
                'owner_id' => 1,
                'executor_id' => 2,
                'created_at' => 1552385960,
                'updated_at' => 1552385960,
            ],
            [
                'id' => 4,
                'name' => 'Sit ipsum ea.',
                'description' => 'Facere qui voluptate accusantium quis expedita ipsa. Incidunt quaerat aliquam et quod. Quisquam non fuga iusto accusantium culpa. Vel architecto inventore velit iusto quis unde.',
                'status' => 30,
                'cost' => 0,
                'owner_id' => 1,
                'executor_id' => 2,
                'created_at' => 1552385960,
                'updated_at' => 1552385960,
            ],
            [
                'id' => 5,
                'name' => 'Et veritatis.',
                'description' => 'Et perspiciatis et eos tenetur perferendis fugit iure. Animi dolorem in autem eos. Ab debitis dolorum in hic odit voluptatum sed.',
                'status' => 10,
                'cost' => 0,
                'owner_id' => 2,
                'executor_id' => null,
                'created_at' => 1552385960,
                'updated_at' => 1552385960,
            ],
            [
                'id' => 6,
                'name' => 'Molestiae sit.',
                'description' => 'Amet suscipit odio iure dolorem voluptatem expedita occaecati maiores. Iure facilis qui et debitis odit.',
                'status' => 10,
                'cost' => 0,
                'owner_id' => 1,
                'executor_id' => 2,
                'created_at' => 1552385960,
                'updated_at' => 1552385960,
            ]

        ]);
    }

    public function showTask(\AcceptanceTester $I)
    {
        $I->sendGET('/tasks/1');
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson([
            'name' => 'Velit ab odio dolor.',
        ]);
    }

    public function notFound(\AcceptanceTester $I) {
        $I->sendGET('/tasks/30');
        $I->seeResponseCodeIs(404);
    }


    public function createTask(\AcceptanceTester $I) {
        $I->sendPOST('/tasks', [
            'name' => 'Lorem ipsum dolor',
            'cost' => 10,
            'owner_id' => 1
        ]);
        $I->seeResponseCodeIs(201);
        $I->seeResponseContainsJson([
            'name' => 'Lorem ipsum dolor',
            'owner_id' => 1
        ]);

    }

    public function update(\AcceptanceTester $I)
    {
        $I->sendPATCH('/tasks/1', [
            'name' => 'Update title',
        ]);
        $I->seeResponseCodeIs(200);

        $I->seeResponseContainsJson([
            'id' => 1,
            'name' => 'Update title',
        ]);
    }

    public function executeTask(\AcceptanceTester $I) {
        $I->amBearerAuthenticated('30Lb-qX5k2i0GIHTZOzD_bqPLp9ze_ve');
        $I->sendPOST('/tasks/execute', [
            'id' => 5
        ]);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'success' => true,
            'executor_id' => 1
        ]);
    }


    public function executeTaskNotFound(\AcceptanceTester $I) {
        $I->amBearerAuthenticated('30Lb-qX5k2i0GIHTZOzD_bqPLp9ze_ve');
        $I->sendPOST('/tasks/execute', [
            'id' => 15
        ]);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'success' => false,
            'message' => 'Task is not found'
        ]);
    }

    public function executeTaskNotNew(\AcceptanceTester $I) {
        $I->amBearerAuthenticated('30Lb-qX5k2i0GIHTZOzD_bqPLp9ze_ve');
        $I->sendPOST('/tasks/execute', [
            'id' => 4
        ]);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'success' => false,
            'message' => 'Task is not new'
        ]);
    }

    public function closeTask(\AcceptanceTester $I) {
        $I->amBearerAuthenticated('30Lb-qX5k2i0GIHTZOzD_bqPLp9ze_ve');
        $I->sendGET('/tasks/close');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }

    public function closeTaskUnauthorized(\AcceptanceTester $I) {
        $I->amBearerAuthenticated('wrong-token');
        $I->sendGET('/tasks/close');
        $I->seeResponseCodeIs(401);
    }
}