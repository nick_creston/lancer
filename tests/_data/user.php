<?php

return [
    'user0' => [
        'id' => 1,
        'username' => 'nader.katarina',
        'password_hash' => '$2y$13$TjNYYUXNQYxmLTDpQn1GLu28amaC0qefOELWfo35pJ/AMSwgPExS6',
        'auth_key' => 'stIsH6uJjD5JgRXDxbCvB0QOsdjRC1nU',
        'access_token' => '30Lb-qX5k2i0GIHTZOzD_bqPLp9ze_ve',
        'status' => 10,
        'created_at' => 1551891191,
        'updated_at' => 1551891191,
    ],
    'user1' => [
        'id' => 2,
        'username' => 'vspencer',
        'password_hash' => '$2y$13$L1.mNDfk7SqSGzpClSTXye6kCLV/W/pyrLlgnNGi0rM3uKeI3PqXq',
        'auth_key' => 'BB35jZa07mLRGyQ8KSlYnlzwLgXJzw_c',
        'access_token' => 'YkP1Q_CWyJdvdArxnvS2iOxMuFrrgZQP',
        'status' => 10,
        'created_at' => 1551891192,
        'updated_at' => 1551891192,
    ],
];
