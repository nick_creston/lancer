<?php

namespace app\tests\fixtures;
use yii\test\ActiveFixture;

class TaskFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Task';
    public $depends = ['app\tests\fixtures\UserFixture'];
    public $dataFile = '@tests/_data/task.php';

    public function unload(){
        parent::unload();
        $this->resetTable();
    }
}