<?php

namespace tests\unit\models;
use app\tests\fixtures\UserFixture;
use app\models\User;

class UserTest extends \Codeception\Test\Unit
{
    protected $tester;
    public function _before()
    {
        $this->tester->haveFixtures([
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ]
        ]);
    }
    public function testFindUserById()
    {
        expect_that($user = User::findIdentity(1));
        expect($user->username)->equals('nader.katarina');

        expect_not(User::findIdentity(999));
    }

    public function testFindUserByAccessToken()
    {
        expect_that($user = User::findIdentityByAccessToken('30Lb-qX5k2i0GIHTZOzD_bqPLp9ze_ve'));
        expect($user->username)->equals('nader.katarina');

        expect_not(User::findIdentityByAccessToken('non-existing'));        
    }

    public function testFindUserByUsername()
    {
        expect_that($user = User::findByUsername('nader.katarina'));
        expect_not(User::findByUsername('not-admin'));
    }

    /**
     * @depends testFindUserByUsername
     */
    public function testValidateUser($user)
    {
        $user = User::findByUsername('nader.katarina');
        expect_that($user->validateAuthKey('stIsH6uJjD5JgRXDxbCvB0QOsdjRC1nU'));
        expect_not($user->validateAuthKey('test102key'));

        expect_that($user->validatePassword('password_0'));
        expect_not($user->validatePassword('123456'));        
    }

}
