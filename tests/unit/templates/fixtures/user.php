<?php
// users.php file under the template path (by default @tests/unit/templates/fixtures)
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */
return [
    'id' => $index,
    'username' => $faker->userName,
    'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('password_' . $index),
    'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
    'access_token' => Yii::$app->getSecurity()->generateRandomString(),
    'status' => 10,
    'created_at' => time(),
    'updated_at' => time(),

];