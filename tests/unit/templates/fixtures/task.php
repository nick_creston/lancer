<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */
return [
    'id' => $index+1,
    'name' => $faker->text(20),
    'description' => $faker->text(200),
    'status' => $faker->randomElement([10, 20, 30]),
    'cost' => 0,
    'owner_id' => 1,
    'executor_id' => 2,
    'created_at' => time(),
    'updated_at' => time(),

];