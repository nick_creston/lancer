<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RegisterForm extends Model
{
    public $username;
    public $password;
    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            ['username', 'unique', 'targetClass'=> User::className()]
        ];
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

    public function register() {
        if(!$this->validate()) {
            return null;
        }
        $user = $this->getUser();
        if(!$user) {
            $new_user = new User();
            $new_user->username = $this->username;
            $new_user->auth_key = Yii::$app->security->generateRandomString();

            $new_user->setPassword($this->password);
            $saved = $new_user->save();
            return $saved ? $new_user : null;
        }
        else {
            return null;
        }
    }
}
