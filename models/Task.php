<?php
namespace app\models;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * Class Task
 * @package app\models
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $status
 * @property integer $cost
 * @property integer $owner_id
 * @property integer $executor_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class Task extends ActiveRecord {
    const STATUS_NEW = 10;
    const STATUS_EXECUTE = 20;
    const STATUS_complete = 30;

    public static function tableName()
    {
        return '{{%tasks}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'cost', 'owner_id'], 'required'],
            ['name', 'string'],
            ['cost', 'integer']

        ];
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    public function getOwner() {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    public function getExecutor() {
        return $this->hasOne(User::className(), ['id' => 'executor_id']);
    }
}